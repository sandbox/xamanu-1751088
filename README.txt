/* $Id: $ */

Summary
============

A text scanning and spam detection framework for Drupal.


Requirements
============

- CTools 1.x
  http://drupal.org/project/ctools
- Drupal 7.x
  http://drupal.org/project/drupal



Installation
============

- Install Libre Spam (librespam), Junk Guardian Admin UI (junkguardian_ui) and Junkguardian Defaults (junkguardian_defaults).
- Navigate to admin/structure/junkguardian.
- Enable one or more scanners, create your own by adding a new one, modify an
  existing one by clicking on 'override' or copy and modify an existing one by
  clicking on 'clone'.


Contact
============

Created and maintained by:
* Felix Delattre (xamanu) - http://drupal.org/user/359937
* Jens Reinemuth (openweb) - http://drupal.org/user/1294566

This project has been sponsored by:
* Erdfisch
  Heidelberg, Germany
  http://www.erdfisch.de
