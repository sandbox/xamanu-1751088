<?php
/**
 *
 * @author openWeb
 */
interface LibrespamProcessorInterface {
  
  public function execute($input);
  public function evaluate();
  
}

?>
