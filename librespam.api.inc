<?php
// $Id: $

/**
 * @file
 *
 */

/**
 * Junk Guardian offers a CTools based plugin API. Scanner, analysers, and processors are
 * declared to Junk Guardian as plugins.
 *
 * @see librespam_plugins()
 * @see LibrespamScanner
 * @see LibrespamAnalyser
 * @see LibrespamProcessor
 *
 * @defgroup pluginapi Plugin API
 * @{
 */

/**
 * Example of a CTools plugin hook that needs to be implemented to make
 * hook_feeds_plugins() discoverable by CTools and Feeds. The hook specifies
 * that the hook_feeds_plugins() returns Feeds Plugin API version 1 style
 * plugins.
 */
function hook_ctools_plugin_api($owner, $api) {
  if ($owner == 'librespam' && $api == 'plugins') {
    return array('version' => 1);
  }
}
