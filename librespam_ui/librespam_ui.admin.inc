<?php
// $Id: $

/**
 * @file
 * Contains all page callbacks and forms Junk Guardian's administrative pages.
 */

/**
 *
 */
function librespam_ui_overview_form(&$form_status) {

  $form = $form['enabled'] = $form['disabled'] = array();

  $form['#header'] = array(
    t('Name'),
    t('Description'),
    t('Attached to'),
    t('Status'),
    t('Operations'),
    t('Enabled'),
  );
  foreach (librespam_scanner_load_all(TRUE) as $scanner) {
    $scanner_form = array();
    $scanner_form['name']['#value'] = $scanner->config['name'];
    $scanner_form['description']['#value'] = $scanner->config['description'];
    if (empty($scanner->config['content_type'])) {
      $scanner_form['attached']['#value'] = '[none]';
    }
    else {
      if (!$scanner->disabled) {
        $scanner_form['attached']['#value'] = l(node_get_types('name', $scanner->config['content_type']), 'node/add/'. str_replace('_', '-', $scanner->config['content_type']));
      }
      else {
        $scanner_form['attached']['#value'] = node_get_types('name', $scanner->config['content_type']);
      }
    }

    if ($scanner->export_type == EXPORT_IN_CODE) {
      $status = t('Default');
      $edit = t('Override');
      $delete = '';
    }
    else if ($scanner->export_type == EXPORT_IN_DATABASE) {
      $status = t('Normal');
      $edit = t('Edit');
      $delete = t('Delete');
    }
    else if ($scanner->export_type == (EXPORT_IN_CODE | EXPORT_IN_DATABASE)) {
      $status = t('Overridden');
      $edit = t('Edit');
      $delete = t('Revert');
    }
    $scanner_form['status'] = array(
      '#value' => $status,
    );
    if (!$scanner->disabled) {
      $scanner_form['operations'] = array(
        '#value' =>
          l($edit, 'admin/build/librespam/edit/'. $scanner->id) .' | '.
          l(t('Export'), 'admin/build/librespam/export/'. $scanner->id) .' | '.
          l(t('Clone'), 'admin/build/librespam/clone/'. $scanner->id) .
          (empty($delete) ? '' :  ' | '. l($delete, 'admin/build/librespam/delete/'. $scanner->id)),
      );
    }
    else {
      $scanner_form['operations']['#value'] = '&nbsp;';
    }

    $scanner_form[$scanner->id] = array(
      '#type' => 'checkbox',
      '#default_value' => !$scanner->disabled,
      '#attributes' => array('class' => 'librespam-ui-trigger-submit'),
    );

    if ($scanner->disabled) {
      $form['disabled'][$scanner->id] = $scanner_form;
    }
    else {
      $form['enabled'][$scanner->id] = $scanner_form;
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array('class' => array('librespam-ui-hidden-submit')),
  );
  return $form;
}

/**
 * Submit handler for librespam_ui_overview_form().
 */
function librespam_ui_overview_form_submit($form, &$form_state) {

}

/**
 * Create a new scanner configuration.
 *
 * @param $form_state
 *  Form API form state array.
 * @param $from_scanner
 *   JunkguardianScanner object. If given, form will create a new scanner as a copy
 *   of $from_scanner.
 */
function librespam_ui_create_form(&$form_state, $from_scanner = NULL) {
  drupal_add_js(drupal_get_path('module', 'librespam_ui') .'/librespam_ui.js');
  $form = array();
  $form['#from_scanner'] = $from_scanner;
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('A natural name for this configuration. You can always change this name later.'),
    '#required' => TRUE,
    '#maxlength' => 128,
    '#attributes' => array('class' => 'scanner-name'),
  );
  $form['id'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine name'),
    '#description' => t('A unique identifier for this configuration. Must only contain lower case characters, numbers and underscores.'),
    '#required' => TRUE,
    '#maxlength' => 128,
    '#attributes' => array('class' => 'scanner-id'),
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('A description of this configuration.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );
  return $form;
}

/**
 * Validation handler for librespam_ui_create_form().
 */
function librespam_ui_create_form_validate($form, &$form_state) {
  ctools_include('export');
  $scanner = librespam_scanner($form_state['values']['id']);
  if (ctools_export_load_object('librespam_scanner', 'conditions', array('id' => $form_state['values']['id']))) {
    form_set_error('id', t('Id is taken.'));
  }
  $scanner->configFormValidate($form_state['values']);
}

/**
 * Submit handler for librespam_ui_create_form().
 */
function librespam_ui_create_form_submit($form, &$form_state) {
  // Create scanner.
  $scanner = librespam_scanner($form_state['values']['id']);
  // If from_scanner is given, copy its configuration.
  if (!empty($form['#from_scanner'])) {
    $scanner->copy($form['#from_scanner']);
  }
  // In any case, we want to set this configuration's title and description.
  $scanner->addConfig($form_state['values']);
  $scanner->save();

  // Set a message and redirect to settings form.
  if (empty($form['#from_scanner'])) {
    drupal_set_message(t('Your configuration has been created with default settings. If they do not fit your use case you can adjust them here.'));
  }
  else {
    drupal_set_message(t('A clone of the !name configuration has been created.', array('!name' => $form['#from_scanner']->config['name'])));
  }
  $form_state['redirect'] = 'admin/build/librespam/edit/'. $scanner->id;
  librespam_cache_clear();
}


/**
 *
 */
function librespam_ui_export_form(&$form_state, $scanner) {

}

/**
 * Submit handler for librespam_ui_export_form().
 */
function librespam_ui_export_form_submit($form, &$form_state) {

}


/**
 *
 */
function librespam_ui_delete_form(&$form_state, $scanner){

}

/**
 * Submit handler for librespam_ui_delete_form().
 */
function librespam_ui_delete_form_submit($form, &$form_state) {

}


/**
 *
 */
function librespam_ui_edit_page($scanner, $active = 'help', $plugin_key = ''){

}




/**
 *
 */
/*
function {

} */


