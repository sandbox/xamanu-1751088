<?php
// $Id:  $

/**
 * @file
 * CTools plugins declarations.
 */

/**
 * Break out for librespam_librespam_plugins().
 */
function _librespam_librespams_plugins() {
  $path = drupal_get_path('module', 'librespam') .'/plugins';

  $info = array();
  $info['LibrespamPlugin'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'class' => 'LibrespamPlugin',
      'file' => 'LibrespamPlugin.inc',
      'path' => $path,
    ),
  );
  $info['LibrespamMissingPlugin'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'class' => 'LibrespamMissingPlugin',
      'file' => 'LibrespamPlugin.inc',
      'path' => $path,
    ),
  );
  $info['LibrespamFetcher'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'parent' => 'LibrespamPlugin',
      'class' => 'LibrespamFetcher',
      'file' => 'LibrespamFetcher.inc',
      'path' => $path,
    ),
  );
  $info['LibrespamAnalyser'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'parent' => 'LibrespamPlugin',
      'class' => 'LibrespamAnalyser',
      'file' => 'LibrespamAnalyser.inc',
      'path' => $path,
    ),
  );
  $info['LibrespamProcessor'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'parent' => 'LibrespamPlugin',
      'class' => 'LibrespamProcessor',
      'file' => 'LibrespamProcessor.inc',
      'path' => $path,
    ),
  );
  $info['JunkguardiaFormFetcher'] = array(
    'name' => 'Form Fetcher',
    'description' => 'Check for spam on forms.',
    'handler' => array(
      'parent' => 'LibrespamFetcher', // This is the key name, not the class name.
      'class' => 'JunkguardiaFormFetcher',
      'file' => 'JunkguardiaFormFetcher.inc',
      'path' => $path,
    ),
  );

  $info['LibrespamBayesAnalyser'] = array(
    'name' => 'Bayes Analyser',
    'description' => 'Bayes logic to detect spam',
    'handler' => array(
      'parent' => 'LibrespamAnalyser',
      'class' => 'LibrespamBayesAnalyser',
      'file' => 'LibrespamBayesAnalyser.inc',
      'path' => $path,
    ),
  );
  $info['LibrespamPublishProcessor'] = array(
    'name' => '(Un-) publish processor',
    'description' => 'React on spam by unpublishing content',
    'help' => '',
    'handler' => array(
      'parent' => 'LibrespamProcessor',
      'class' => 'LibrespamPublishProcessor',
      'file' => 'LibrespamPublishProcessor.inc',
      'path' => $path,
    ),
  );  return $info;
}
