<?php
// $Id: $

/**
 * @file
 * Junk Guardian - basic API functions and hook implementations.
 */

// An object that is not persistent. Compare EXPORT_IN_DATABASE, EXPORT_IN_CODE.
define('LIBRESPAM_EXPORT_NONE', 0x0);


/**
 * @defgroup hooks Hook and callback implementations
 * @{
 */

/**
 * Implements hook_help().
 */
function librespam_help($path, $arg) {
  switch ($path) {
    case 'admin/config/librespam':
      return t('TODO');
  }
}

/**
 * Implements hook_permission().
 */
function librespam_permission() {
  return array(
    'moderate librespam' => array(
      'title' => t('Moderate spam content'),
      'description' => t('View and moderate content detected as spam.'),
    ),
    'manage librespam' => array(
      'title' => t('Manage spam detection'),
      'description' => t('Create and edit spam detection rules.'),
    ),
    'administer librespam' => array(
      'title' => t('Administer settings'),
      'description' => t('Administer the general settings.'),
    ),
  );
}


/**
 * Implements hook_menu().
 */
function librespam_menu() {

/* Do we really need a general settings page?

  $items['admin/config/librespam'] = array(
    'title' => 'Junk detection',
    'description' => 'General settings for Junk Guardian',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('librespam_admin_settings'),
    'access arguments' => array('administer librespam'),
    'file' => 'librespam.admin.inc',
  );
*/
  $items['admin/reports/librespam'] = array(
    'title' => 'Spam detection',
    'description' => 'Logs and statistics about spam detection',
    'page callback' => 'librespam_reports_overview',
    'access arguments' => array('moderate librespam'),
    'file' => 'librespam.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function librespam_views_api() {
  return array(
    'api' => '2',
    'path' => drupal_get_path('module', 'librespam') .'/views',
  );
}

/**
 * Implementation of hook_ctools_plugin_api().
 */
function librespam_ctools_plugin_api($owner, $api) {
  if ($owner == 'librespam' && $api == 'plugins') {
    return array('version' => 1);
  }
}

/**
 * Implementation of hook_librespam_plugins().
 */
function librespam_librespam_plugins() {
  module_load_include('inc', 'librespam', 'librespam.plugins');
  return _librespam_librespam_plugins();
}

/**
 * @}
 */

/**
 * @defgroup utility Utility functions
 * @{
 */

/**
 * Loads all scanners.
 *
 * @param $load_disabled
 *   Pass TRUE to load all scanners, enabled or disabled, pass FALSE to only
 *   retrieve enabled scanners.
 *
 * @return
 *   An array of all librespam's configurations available.
 */
function librespam_scanner_load_all($load_disabled = FALSE) {
  $scanner = array();
  // This function can get called very early in install process through
  // menu_router_rebuild(). Do not try to include CTools if not available.
  if (function_exists('ctools_include')) {
    ctools_include('export');
    $configs = ctools_export_load_object('librespam_scanner', 'all');
    foreach ($configs as $config) {
      if (!empty($config->id) && ($load_disabled || empty($config->disabled))) {
        $librespam[$config->id] = librespam_scanner($config->id);
      }
    }
  }
  return $scanner;
}

/**
 * Gets an array of enabled scanner ids.
 *
 * @return
 *   An array where the values contain ids of enabled scanners.
 */
function librespam_enabled_scanner() {
  return array_keys(_librespam_scanner_digest());
}

/**
 * Gets an enabled scanner configuration by content type.
 *
 * @param $content_type
 *   A node type string.
 *
 * @return
 *   A JunkguardianScanner id if there is an scanner for the given content type,
 *   FALSE otherwise.
 */
function librespam_get_scanner_id($content_type) {
  $scanners = array_flip(_librespam_scanner_digest());
  return isset($scanners[$content_type]) ? $scanners[$content_type] : FALSE;
}

/**
 * Helper function for librespam_get_scanner_id() and librespam_enabled_scanners().
 */
function _librespam_scanner_digest() {
  $scanners = &ctools_static(__FUNCTION__);
  if ($scanners === NULL) {
    if ($cache = cache_get(__FUNCTION__)) {
      $scanners = $cache->data;
    }
    else {
      $scanners = array();
      foreach (librespam_scanner_load_all() as $scanner) {
        $scanners[$scanner->id] = $scanner->config['content_type'];
      }
      cache_set(__FUNCTION__, $scanners);
    }
  }
  return $scanners;
}

/**
 * Resets scanner caches. Call when enabling/disabling scanners.
 */
function librespam_cache_clear($rebuild_menu = TRUE) {
  cache_clear_all('_librespam_scanner_digest', 'cache');
  ctools_static_reset('_librespam_scanner_digest');
  ctools_include('export');
  ctools_export_load_object_reset('librespam_scanner');
  node_get_types('types', NULL, TRUE);
  if ($rebuild_menu) {
    menu_rebuild();
  }
}

/**
 * Exports a JunkguardianScanner configuration to code.
 */
function librespam_export($scanner_id, $indent = '') {
  ctools_include('export');
  $result = ctools_export_load_object('librespam_scanner', 'names', array('id' => $scanner_id));
  if (isset($result[$scanner_id])) {
    return ctools_export_object('librespam_scanner', $result[$scanner_id], $indent);
  }
}

/**
 * Logs to a file like /mytmp/librespam_my_domain_org.log in temporary directory.
 */
function librespam_dbg($msg) {
  if (variable_get('librespam_debug', false)) {
    if (!is_string($msg)) {
      $msg = var_export($msg, true);
    }
    $filename = trim(str_replace('/', '_', $_SERVER['HTTP_HOST'] . base_path()), '_');
    $handle = fopen(file_directory_temp() ."/librespam_$filename.log", 'a');
    fwrite($handle, date('c') ."\t$msg\n");
    fclose($handle);
  }
}

/**
 * @}
 */


/**
 * Includes a librespam module include file.
 *
 * @param $file
 *   The filename without the .inc extension.
 * @param $directory
 *   The directory to include the file from. Do not include files from libraries
 *   directory. Use librespam_include_library() instead
 */
function librespam_include($file, $directory = 'includes') {
  static $included = array();
  if (!isset($included[$file])) {
    require './'. drupal_get_path('module', 'librespam') ."/$directory/$file.inc";
  }
  $included[$file] = TRUE;
}

/**
 * @}
 */


/**
 * @defgroup instantiators Instantiators
 * @{
 */

/**
 * Gets an scanner instance.
 *
 * @param $id
 *   The unique id of the scanner object.
 *
 * @return
 *   A JunkguardianScanner object or an object of a class defined by the Drupal
 *   variable 'librespam_scanner_class'. There is only one scanner object
 *   per $id system-wide.
 */
function librespam_scanner($id) {
  librespam_include('JunkguardianScanner');
  return JunkguardianConfigurable::instance(variable_get('librespam_scanner_class', 'JunkguardianScanner'), $id);
}

/**
 * @}
 */


/**
 * @defgroup plugins Plugin functions
 * @{
 *
 * @todo Encapsulate this in a JunkguardianPluginHandler class, move it to includes/
 * and only load it if we're manipulating plugins.
 */

/**
 * Gets all available plugins. Does not list hidden plugins.
 *
 * @return
 *   An array where the keys are the plugin keys and the values
 *   are the plugin info arrays as defined in hook_librespam_plugins().
 */
function librespam_get_plugins() {
  ctools_include('plugins');
  $plugins = ctools_get_plugins('librespam', 'plugins');

  $result = array();
  foreach ($plugins as $key => $info) {
    if (!empty($info['hidden'])) {
      continue;
    }
    $result[$key] = $info;
  }

  // Sort plugins by name and return.
  uasort($result, 'librespam_plugin_compare');
  return $result;
}

/**
 * Sort callback for librespam_get_plugins().
 */
function librespam_plugin_compare($a, $b) {
  return strcasecmp($a['name'], $b['name']);
}

/**
 * Gets all available plugins of a particular type.
 *
 * @param $type
 *   'fetcher', 'analyser' or 'processor'
 */
function librespam_get_plugins_by_type($type) {
  $plugins = librespam_get_plugins();

  $result = array();
  foreach ($plugins as $key => $info) {
    if ($type == librespam_plugin_type($key)) {
      $result[$key] = $info;
    }
  }
  return $result;
}

/**
 * Gets an instance of a class for a given plugin and id.
 *
 * @param $plugin
 *   A string that is the key of the plugin to load.
 * @param $id
 *   A string that is the id of the object.
 *
 * @return
 *   A JunkguardianPlugin object.
 *
 * @throws Exception
 *   If plugin can't be instantiated.
 */
function librespam_plugin_instance($plugin, $id) {
  librespam_include('JunkguardianScanner');
  ctools_include('plugins');
  if ($class = ctools_plugin_load_class('librespam', 'plugins', $plugin, 'handler')) {
    return JunkguardianConfigurable::instance($class, $id);
  }
  drupal_set_message(t('Missing Junkguardian plugin. Check whether all required libraries and modules are installed properly.'), 'warning');
  $class = ctools_plugin_load_class('librespam', 'plugins', 'JunkguardianMissingPlugin', 'handler');
  return JunkguardianConfigurable::instance($class, $id);
}

/**
 * Determines whether given plugin is derived from given base plugin.
 *
 * @param $plugin_key
 *   String that identifies a Junkguardian plugin key.
 * @param $parent_plugin
 *   String that identifies a Junkguardian plugin key to be tested against.
 *
 * @return
 *   TRUE if $parent_plugin is directly *or indirectly* a parent of $plugin,
 *   FALSE otherwise.
 */
function librespam_plugin_child($plugin_key, $parent_plugin) {
  ctools_include('plugins');
  $plugins = ctools_get_plugins('librespam', 'plugins');
  $info = $plugins[$plugin_key];

  if (empty($info['handler']['parent'])) {
    return FALSE;
  }
  elseif ($info['handler']['parent'] == $parent_plugin) {
    return TRUE;
  }
  else {
    return librespam_plugin_child($info['handler']['parent'], $parent_plugin);
  }
}

/**
 * Determines the type of a plugin.
 *
 * @param $plugin_key
 *   String that identifies a Junkguardian plugin key.
 *
 * @return
 *   One of the following values:
 *   'fetcher' if the plugin is a fetcher
 *   'analyser' if the plugin is a analyser
 *   'processor' if the plugin is a processor
 *   FALSE otherwise.
 */
function librespam_plugin_type($plugin_key) {
  if (librespam_plugin_child($plugin_key, 'JunkguardianFetcher')) {
    return 'fetcher';
  }
  elseif (librespam_plugin_child($plugin_key, 'JunkguardianAnalyser')) {
    return 'analyser';
  }
  elseif (librespam_plugin_child($plugin_key, 'JunkguardianProcessor')) {
    return 'processor';
  }
  return FALSE;
}

  
  /**
   * @todo: Implement rules API 
   */
  function librespam_do_reaction() {
    error_log('Libre Spam reaction executed');
  }