<?php

/**
 * Plugin class, defines shared functionality between fetchers.
 *
 */
class LibrespamContentControllerNodes extends LibrespamContentControllerGeneric implements LibrespamContentControllerInterface {

  /**
   * convert node object into continuous string 
   */
  public function prepare_input($node) {
    
    // get information about fields of this node-type
    $fields = field_info_instances('node', $node->type);
    
    $this->input = $node->title;
    // concatenate fields values into single string 
    foreach($fields as $field_name => $field_definition) {
      $field_value = field_view_field('node', $node, $field_name);
      $this->input .= ' ' . strip_tags(drupal_render($field_value));
    }
  }
  
  public function serve_input() {
    return $this->input;
  }
}
